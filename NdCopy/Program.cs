﻿using System;

namespace NdCopy
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                NdCopy.Run();
                Console.WriteLine(@"All Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
