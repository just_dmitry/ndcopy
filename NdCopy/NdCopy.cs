﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NdCopy.Properties;

namespace NdCopy
{
    public class NdCopy
    {
        private static readonly DirectoryInfo SourceDir = new DirectoryInfo(@"E:\NDMZ\FGUFSND");
        private static readonly DirectoryInfo DestinationDir = new DirectoryInfo(@"E:\NDMZ\FGUFSND_RZN");

        private static readonly StringBuilder OutfileBuilder = new StringBuilder();

        private static Dictionary<string, string> data;

        public static void Run()
        {
            data = Resources.NDList.Split('\n').Select(x => x.Split('\t')).ToDictionary(x => x[0], x => string.Join("\t", x));
            ProcessFolder(SourceDir);

            File.WriteAllText(Path.Combine(DestinationDir.FullName, "list.csv"), OutfileBuilder.ToString());
            Console.WriteLine(@"NdCopy Done");
        }

        private static void ProcessFolder(DirectoryInfo dir)
        {
            // usubdirs
            foreach (var subdir in dir.GetDirectories())
            {
                ProcessFolder(subdir);
            }

            // current dir
            var nameParts = dir.Name.Split('_');
            if (nameParts.Length != 4)
            {
                return;
            }

            string docInfo;
            if (!data.TryGetValue(nameParts[2], out docInfo))
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(@"НД не распознан: {0}", nameParts[2]);
                Console.ResetColor();
                return;
            }

            var newSubdir = new DirectoryInfo(Path.Combine(DestinationDir.FullName, nameParts[2]));
            if (!newSubdir.Exists)
            {
                newSubdir.Create();
            }
            var count = 0;
            foreach (var file in dir.GetFiles())
            {
                var newFile = Path.Combine(newSubdir.FullName, file.Name);
                if (!File.Exists(newFile))
                {
                    file.CopyTo(newFile, false);
                }
                count++;
            }
            OutfileBuilder.AppendLine(docInfo);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(@"НД {0}: успешно скопировано {1} файлов", nameParts[2], count);
            Console.ResetColor();
        }
    }
}
